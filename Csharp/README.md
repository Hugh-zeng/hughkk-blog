# C#代码记录







opencv笔记

测试是否可以同时写置位两个y端  补充一个弹窗 提示方案在加载中 加载完成则关闭

把作业做了。



### 使用json做序列

步骤

下载



## 文件操作

```c#
//获取OK目录下的子目录
            DirectoryInfo directoryInfo = new DirectoryInfo(@"D:\检测点A\OK");
            //返回OK目录下的所有子目录
            DirectoryInfo[] infos = directoryInfo.GetDirectories();
            Console.WriteLine(infos.Length);
            //如果子目录有2个或者大于2个文件所在则删除久的一个 （排序如果是时间按照时间来）
            if (infos.Length>=2)
            {
                infos[0].Delete(true);//删除目录下的所有文件和子目录
            }
```



## 线程

#### 计时线程：



#### 线程等待

[C#多线程(11)：线程等待 - 云+社区 - 腾讯云 (tencent.com)](https://cloud.tencent.com/developer/article/1817853)

![](../images/image-20211220195437992.png)





#### 硬盘信息获取

https://www.cnblogs.com/hnsongbiao/p/9160614.html 获取计算机硬件信息

```C#
//获取的是long型 是字节型 
//比如 c盘 有9.8G空间 它获取出的long值是10520989696字节 在乘以 （1024*1024*1024） 结果是 9.8G 
drive.TotalSize();
```

![](../images/image-20211221000514458.png)

